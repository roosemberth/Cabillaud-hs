{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ViewPatterns #-}

module Cabillaud.Types where

import Data.Aeson
import Data.Aeson.TH (deriveJSON)
import Data.Aeson.Types (FromJSON)
import Data.Char (toLower)
import Data.Text (Text)
import Data.Time (Day)
import GHC.Generics (Generic)

type Username = Text

data User = User
  { username :: Username
  , displayName :: Text
  , screenName :: Text
  } deriving (Eq, Show, Generic, FromJSON, ToJSON)

type GroupId = Text

data Group = Group
  { name :: Text
  , description :: Text
  , users :: [Username]
  } deriving (Eq, Show, Generic, ToJSON)

-- | FromJSON instance allowing empty users.
instance FromJSON Group where
  parseJSON = withObject "Group" $ \o ->
    Group <$> o .: "name" <*> o .: "description" <*> o .:? "users" .!= []

type Schedule = Text

type ItemId = Text

data Item = Item
  { group :: GroupId
  , name :: Text
  , category :: Category
  , tags :: [Text]
  , availableFrom :: Maybe (Day, Schedule)
  , availableTo :: Maybe (Day, Schedule)
  , dependencies :: [ItemDependency]
  } deriving (Eq, Show, Generic, FromJSON, ToJSON)

type Category = Text

data DependencyType = Suggestion | Requirement
  deriving (Eq, Show, Generic, FromJSON, ToJSON)

newtype ItemDependency = ItemDependency (ItemId, DependencyType)
  deriving stock (Eq, Show)
  deriving newtype (FromJSON, ToJSON)

type LoanRequestId = Text

data LoanRequestStatus =
    LoanRequestStatusDraft
  | LoanRequestStatusSubmitted
  | LoanRequestStatusChangesRequested
  | LoanRequestStatusAccepted
  | LoanRequestStatusEnacted
  | LoanRequestStatusArchived
  deriving (Eq, Show, Generic)

$(deriveJSON
  defaultOptions{ constructorTagModifier = map toLower . drop 16 }
  ''LoanRequestStatus)

data LoanRequest = LoanRequest
  { requester :: Username
  , group :: Maybe GroupId
  , loanTeller :: Username
  , loanCourier :: Username
  , loanDate :: Day
  , loanSchedule :: Schedule
  , returnTeller :: Maybe Username
  , returnCourier :: Maybe Username
  , returnDate :: Day
  , returnSchedule :: Schedule
  , objects :: [Item]
  , status :: LoanRequestStatus
  } deriving (Eq, Show, Generic, FromJSON, ToJSON)

