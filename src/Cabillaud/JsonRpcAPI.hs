{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}

module Cabillaud.JsonRpcAPI where

import Cabillaud.Types
import Control.Applicative (liftA2)
import Data.Aeson
import Data.HashMap.Strict (insert)
import Data.Text (Text)
import Data.Time (Day)
import GHC.Generics (Generic)
import Servant.API
import Servant.JsonRpc

-- | Transport type wrapping an object and its id.
-- The object id will be embedded in its json representation.
data WithId a = WithId Text a
  deriving (Eq, Show)

instance ToJSON a => ToJSON (WithId a) where
  toJSON (WithId id v) = appendKey (toJSON id) (toJSON v)
    where appendKey :: Value -> Value -> Value
          appendKey k (Object props) = Object (insert "id" k props)
          appendKey k v = object [("id", k), ("value", v)]

instance FromJSON a => FromJSON (WithId a) where
  parseJSON = withObject "Object with id" $
    \o -> liftA2 WithId (o .: "id") (parseJSON (Object o))

newtype Err = Err Text
  deriving (Eq, Show, Generic)
  deriving anyclass (ToJSON, FromJSON)

-- | API not requiring an authenticated user
type PublicJsonRpc =
       JsonRpc "users.get"    Username Err User
  :<|> JsonRpc "users.create" User     Err ()
  :<|> JsonRpc "users.search" Text Err [User]
  :<|> JsonRpc "groups.get"    GroupId  Err Group
  :<|> JsonRpc "groups.search" Text Err [Group]
  :<|> JsonRpc "items.get"    ItemId Err Item
  :<|> JsonRpc "items.search" Text Err [Item]

data NewLoanRequest = NewLoanRequest
  { group :: Maybe GroupId
  , loanDate :: Day
  , loanSchedule :: Schedule
  , returnDate :: Day
  , returnSchedule :: Schedule
  , objects :: [ItemId]
  } deriving (Eq, Show, Generic, FromJSON, ToJSON)

-- | API requiring user authentication
-- Assumes the currently authenticated 'User' is somehow available.
type PrivateJsonRpc =
    -- | The field username is inmutable
       JsonRpc "users.update"    User Err ()
  :<|> JsonRpc "users.getgroups" Username Err [WithId Group]
  :<|> JsonRpc "groups.create" Group Err (WithId Group)
  :<|> JsonRpc "groups.modify" (WithId Group) Err (WithId Group)
  :<|> JsonRpc "items.create" Item Err (WithId Item)
  :<|> JsonRpc "items.modify" (WithId Item) Err (WithId Item)
  :<|> JsonRpc "loanrequests.create" NewLoanRequest Err (WithId LoanRequest)
  :<|> JsonRpc "loanrequests.get"    LoanRequestId Err (WithId LoanRequest)
  :<|> JsonRpc "loanrequests.update" (WithId LoanRequest) Err (WithId LoanRequest)
  :<|> JsonRpc "loanrequests.list"   GroupId Err [Item]

type PublicJsonRpcAPI = RawJsonRpc PublicJsonRpc
type PrivateJsonRpcAPI = RawJsonRpc PrivateJsonRpc
