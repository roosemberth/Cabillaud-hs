{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ConstraintKinds #-}
module Cabillaud.Server.JsonRpcPublic where

import Cabillaud.JsonRpcAPI (Err, PublicJsonRpcAPI)
import Cabillaud.Server.Types (SrvError(..))
import Cabillaud.Types
import Control.Monad.Except
import Data.Text (Text)
import Servant
import Servant.Server.JsonRpc (JsonRpcErr)

type RpcEpHandler m = MonadError SrvError m

-- | Remaps an SrvError-wrapped RpcError into an RPC response.
remapRpcError :: MonadError SrvError m => m a -> m (Either (JsonRpcErr Err) a)
remapRpcError handler = catchError (Right <$> handler) remap
  where remap (RpcError e) = pure (Left e)
        remap e            = throwError e

jsonRpcPublicServer :: RpcEpHandler m => ServerT PublicJsonRpcAPI m
jsonRpcPublicServer = remapRpcError . rpcUsersGet
                 :<|> remapRpcError . rpcUsersCreate
                 :<|> remapRpcError . rpcUsersSearch
                 :<|> remapRpcError . rpcGroupsGet
                 :<|> remapRpcError . rpcGroupsSearch
                 :<|> remapRpcError . rpcItemsGet
                 :<|> remapRpcError . rpcItemsSearch

rpcUsersGet :: RpcEpHandler m => Username -> m User
rpcUsersGet = undefined

rpcUsersCreate :: RpcEpHandler m => User -> m ()
rpcUsersCreate = undefined

rpcUsersSearch :: RpcEpHandler m => Text -> m [User]
rpcUsersSearch = undefined

rpcGroupsGet :: RpcEpHandler m => GroupId -> m Group
rpcGroupsGet = undefined

rpcGroupsSearch :: RpcEpHandler m => Text -> m [Group]
rpcGroupsSearch = undefined

rpcItemsGet :: RpcEpHandler m => ItemId -> m Item
rpcItemsGet = undefined

rpcItemsSearch :: RpcEpHandler m => Text -> m [Item]
rpcItemsSearch = undefined
