module Cabillaud.Server where

import Cabillaud.JsonRpcAPI
import Cabillaud.Server.JsonRpcPublic (jsonRpcPublicServer)
import Cabillaud.Server.Types
import Cabillaud.Util.Convertible
import Control.Monad.Except
import Data.Bifunctor (Bifunctor(first))
import Data.Functor.Identity (runIdentity)
import Data.Proxy
import Servant

-- | FIXME: This API type should be completed
type CabillaudAPI = PublicJsonRpcAPI

cabillaudAPI :: Proxy CabillaudAPI
cabillaudAPI = Proxy

server :: ServerT CabillaudAPI SrvHandler
server = jsonRpcPublicServer

servantServer :: SrvHoists -> Server CabillaudAPI
servantServer hoists = hoistServer cabillaudAPI (nt hoists) server
    where nt :: SrvHoists -> SrvHandler a -> Handler a
          nt hoists handler = Handler { runHandler' = ExceptT transformed }
            where transformed = srvHandlerNt hoists handler

srvHandlerNt :: SrvHoists -> SrvHandler a -> IO (Either Servant.ServerError a)
srvHandlerNt _ =
  pure
  . first convert
  . runIdentity
  . runExceptT
