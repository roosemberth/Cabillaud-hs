# Cabillaud-hs

Various tools for project [Cabillaud][Cabillaud]. The project scope is to
provide test implementations for both the Client and Server, but it may evolve
into an independent implementation.

[Cabillaud]: https://gitlab.com/manduweb13/cabillaud
