{-# LANGUAGE MultiParamTypeClasses #-}
module Cabillaud.Util.Convertible where

class Convertible a b where
  convert :: a -> b
