{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE StandaloneDeriving #-}

module Cabillaud.Server.Database where

import Data.Text (Text)
import Database.Beam
import Database.Beam.Backend.SQL.Types (SqlSerial)
import Data.Int (Int32)
import Data.Time (TimeOfDay, Day)

data ItemT f = Item
  { _itemId           :: Columnar f Int32
  , _itemCategoryName :: PrimaryKey CategoryT f
  , _itemGroup        :: PrimaryKey GrpT f
  , _itemName         :: Columnar f Text
  , _itemValidFrom    :: Columnar f Day
  , _itemValidTo      :: Columnar (Nullable f) Day
  } deriving (Generic, Beamable)
type Item = ItemT Identity
deriving instance Show Item

instance Table ItemT where
  data PrimaryKey ItemT f = ItemId (Columnar f Int32)
    deriving (Generic, Beamable)
  primaryKey = ItemId . _itemId
deriving instance Show (PrimaryKey ItemT Identity)

data ItemDependencyT f = ItemDependency
  { _itemDependencyId        :: Columnar f Int32
  , _itemDependencyItem      :: PrimaryKey ItemT f
  , _itemDependencyDependsOn :: PrimaryKey ItemT f
  } deriving (Generic, Beamable)
type ItemDependency = ItemDependencyT Identity
deriving instance Show ItemDependency

instance Table ItemDependencyT where
  data PrimaryKey ItemDependencyT f = ItemDependencyId (Columnar f Int32)
    deriving (Generic, Beamable)
  primaryKey = ItemDependencyId . _itemDependencyId
deriving instance Show (PrimaryKey ItemDependencyT Identity)

data Item'LoanRequestT f = Item'LoanRequest
  { _item'LoanRequestItem        :: PrimaryKey ItemT f
  , _item'LoanRequestLoanRequest :: PrimaryKey LoanRequestT f
  } deriving (Generic, Beamable)
type Item'LoanRequest = Item'LoanRequestT Identity
deriving instance Show Item'LoanRequest

instance Table Item'LoanRequestT where
  data PrimaryKey Item'LoanRequestT f =
    Item'LoanRequestKey (PrimaryKey ItemT f) (PrimaryKey LoanRequestT f)
    deriving (Generic, Beamable)
  primaryKey Item'LoanRequest{..} =
    Item'LoanRequestKey _item'LoanRequestItem _item'LoanRequestLoanRequest
deriving instance Show (PrimaryKey Item'LoanRequestT Identity)

newtype CategoryT f = CategoryT
  { _categoryName :: Columnar f Text
  } deriving (Generic, Beamable)
type Category = CategoryT Identity
deriving instance Show Category

instance Table CategoryT where
  data PrimaryKey CategoryT f = CategoryName (Columnar f Text)
    deriving (Generic, Beamable)
  primaryKey = CategoryName . _categoryName
deriving instance Show (PrimaryKey CategoryT Identity)

data SubCategoryT f = SubCategory
  { _subCategoryId           :: Columnar f Int32
  , _subCategoryTag          :: Columnar f Text
  , _subCategoryCategoryName :: PrimaryKey CategoryT f
  } deriving (Generic, Beamable)
type SubCategory = SubCategoryT Identity
deriving instance Show SubCategory

instance Table SubCategoryT where
  data PrimaryKey SubCategoryT f = SubCategoryId (Columnar f Int32)
    deriving (Generic, Beamable)
  primaryKey = SubCategoryId . _subCategoryId
deriving instance Show (PrimaryKey SubCategoryT Identity)

data Item'SubCategoryT f = Item'SubCategory
  { _Item'SubCategoryItem        :: PrimaryKey ItemT f
  , _Item'SubCategorySubCategory :: PrimaryKey SubCategoryT f
  } deriving (Generic, Beamable)

instance Table Item'SubCategoryT where
  data PrimaryKey Item'SubCategoryT f =
    Item'SubCategoryKey (PrimaryKey ItemT f) (PrimaryKey SubCategoryT f)
    deriving (Generic, Beamable)
  primaryKey Item'SubCategory{..} =
    Item'SubCategoryKey _Item'SubCategoryItem _Item'SubCategorySubCategory
deriving instance Show (PrimaryKey Item'SubCategoryT Identity)

data Item'BasketT f = Item'Basket
  { _item'basketUsername :: PrimaryKey UserT f
  , _item'basketItem     :: PrimaryKey ItemT f
  } deriving (Generic, Beamable)
type Item'Basket = Item'BasketT Identity
deriving instance Show Item'Basket

instance Table Item'BasketT where
  data PrimaryKey Item'BasketT f =
    Item'BasketKey (PrimaryKey UserT f) (PrimaryKey ItemT f)
    deriving (Generic, Beamable)
  primaryKey Item'Basket{..} =
    Item'BasketKey _item'basketUsername _item'basketItem
deriving instance Show (PrimaryKey Item'BasketT Identity)

data UserT f = User
  { _userUsername    :: Columnar f Text
  , _userDisplayName :: Columnar f Text
  , _userScreenName  :: Columnar f Text
  } deriving (Generic, Beamable)
type User = UserT Identity
deriving instance Show User

instance Table UserT where
  data PrimaryKey UserT f = UserKey (Columnar f Text)
    deriving (Generic, Beamable)
  primaryKey = UserKey . _userUsername
deriving instance Show (PrimaryKey UserT Identity)

data LoanRequestStatus
  = Draft
  | Submitted
  | ChangesRequested
  | Accepted
  | Enacted
  | Archived
  deriving (Show, Read, Eq, Ord, Enum)

data LoanRequestT f = LoanRequest
  { _loanRequestId             :: Columnar f Int32
  , _loanRequestAuthor         :: PrimaryKey UserT f
  , _loanRequestGrp            :: PrimaryKey GrpT f
  , _loanRequestLoanCourier    :: PrimaryKey UserT f
  , _loanRequestLoanDate       :: Columnar f Day
  , _loanRequestLoanSchedule   :: PrimaryKey ScheduleT f
  , _loanRequestLoanTeller     :: PrimaryKey UserT f
  , _loanRequestReturnCourier  :: PrimaryKey UserT f
  , _loanRequestReturnDate     :: Columnar f Day
  , _loanRequestReturnSchedule :: PrimaryKey ScheduleT f
  , _loanRequestReturnTeller   :: PrimaryKey UserT f
  , _loanRequestStatus         :: Columnar f LoanRequestStatus
  } deriving (Generic, Beamable)
type LoanRequest = LoanRequestT Identity
deriving instance Show LoanRequest

instance Table LoanRequestT where
  data PrimaryKey LoanRequestT f = LoanRequestId (Columnar f Int32)
    deriving (Generic, Beamable)
  primaryKey = LoanRequestId . _loanRequestId
deriving instance Show (PrimaryKey LoanRequestT Identity)

data GrpT f = Grp
  { _grpId          :: Columnar f Int32
  , _grpName        :: Columnar f Text
  , _grpDescription :: Columnar (Nullable f) Text
  } deriving (Generic, Beamable)
type Grp = GrpT Identity
deriving instance Show Grp

instance Table GrpT where
  data PrimaryKey GrpT f = GrpId (Columnar f Int32)
    deriving (Generic, Beamable)
  primaryKey = GrpId . _grpId
deriving instance Show (PrimaryKey GrpT Identity)

data User'GrpT f = User'Grp
  { _user'GrpUsername              :: PrimaryKey UserT f
  , _user'GrpGrp                   :: PrimaryKey GrpT f
  , _user'GrpCanAcceptLoanRequests :: Columnar f Bool
  , _user'GrpCanAddOrRemoveUsers   :: Columnar f Bool
  , _user'GrpCanCreateLoanRequests :: Columnar f Bool
  , _user'GrpCanManageUserRights   :: Columnar f Bool
  , _user'GrpCanModifyLoanRequests :: Columnar f Bool
  } deriving (Generic, Beamable)
type User'Grp = User'GrpT Identity
deriving instance Show User'Grp

instance Table User'GrpT where
  data PrimaryKey User'GrpT f = User'GrpKey (PrimaryKey UserT f) (PrimaryKey GrpT f)
    deriving (Generic, Beamable)
  primaryKey User'Grp{..} = User'GrpKey _user'GrpUsername _user'GrpGrp
deriving instance Show (PrimaryKey User'GrpT Identity)

data ScheduleT f = Schedule
  { _scheduleId        :: Columnar f Int32
  , _scheduleName      :: Columnar f Text
  , _scheduleTimeStart :: Columnar f TimeOfDay
  , _scheduleTimeEnd   :: Columnar f TimeOfDay
  , _scheduleValidFrom :: Columnar f Day
  , _scheduleValidTo   :: Columnar (Nullable f) Day
  } deriving (Generic, Beamable)
type Schedule = ScheduleT Identity
deriving instance Show Schedule

instance Table ScheduleT where
  data PrimaryKey ScheduleT f = ScheduleId (Columnar f Int32)
    deriving (Generic, Beamable)
  primaryKey = ScheduleId . _scheduleId
deriving instance Show (PrimaryKey ScheduleT Identity)

data CabillaudDb f = CabillaudDb
  { _dbItem             :: f (TableEntity ItemT)
  , _dbItemDependency   :: f (TableEntity ItemDependencyT)
  , _dbItem'LoanRequest :: f (TableEntity Item'LoanRequestT)
  , _dbSubCategory      :: f (TableEntity SubCategoryT)
  , _dbItem'SubCategory :: f (TableEntity Item'SubCategoryT)
  , _dbItem'Basket      :: f (TableEntity Item'BasketT)
  , _dbUser             :: f (TableEntity UserT)
  , _dbLoanRequest      :: f (TableEntity LoanRequestT)
  , _dbGrp              :: f (TableEntity GrpT)
  , _dbUser'Grp         :: f (TableEntity User'GrpT)
  , _dbSchedule         :: f (TableEntity ScheduleT)
  } deriving (Generic, Database be)

cabillaudDb :: DatabaseSettings be CabillaudDb
cabillaudDb = defaultDbSettings
