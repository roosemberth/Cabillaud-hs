{-# LANGUAGE MultiParamTypeClasses #-}
module Cabillaud.Server.Types where

import Cabillaud.JsonRpcAPI (Err)
import Cabillaud.Util.Convertible
import Control.Monad.Except
import Data.Functor.Identity (Identity)
import Servant
import Servant.Server.JsonRpc

data SrvError =
    RpcError (JsonRpcErr Err)
  | UnspecifiedErr

instance Convertible SrvError ServerError where
  convert (RpcError msg) = undefined

type SrvHandler = ExceptT SrvError Identity

data SrvHoists = SrvHoists
